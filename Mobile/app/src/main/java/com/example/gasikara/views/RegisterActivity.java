package com.example.gasikara.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.gasikara.R;
import com.example.gasikara.models.payloads.RegisterPayload;
import com.example.gasikara.models.responses.LoginResponse;
import com.example.gasikara.models.responses.RegisterResponse;
import com.example.gasikara.services.ApiClient;
import com.example.gasikara.tools.Notification;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends BaseNoActionBarActivity {
    // Fields
    private EditText ETusername;
    private EditText ETpassword;
    private EditText ETpasswordConfirm;
    private Button BTNRegister;

    private ProgressBar progressBar;

    private void init(){
        setContentView(R.layout.activity_register);

        progressBar = findViewById(R.id.progressBar);

        ETusername = findViewById(R.id.username);
        ETpassword = findViewById(R.id.password);
        ETpasswordConfirm = findViewById(R.id.password_confirm);
        BTNRegister = findViewById(R.id.buttonRegister);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.init();

        BTNRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RegisterPayload payload = new RegisterPayload(ETusername.getText().toString(), ETpassword.getText().toString(), ETpasswordConfirm.getText().toString());

                if (TextUtils.isEmpty(payload.getUsername()) || TextUtils.isEmpty(payload.getPassword()) || TextUtils.isEmpty(payload.getPasswordConfirm()) ){
                    Toast.makeText(RegisterActivity.this, "Username / Password Required", Toast.LENGTH_LONG).show();
                }else {
                    register(payload);
                }
            }
        });
    }

    private void register(RegisterPayload payload){
        showLoadingSpinner();


        Call<RegisterResponse> loginResponseCall = ApiClient.getInstance(getApplicationContext()).getUserService().userRegister(payload);
        loginResponseCall.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                try {
                    if (response.isSuccessful()){
                        Notification.showNotification(RegisterActivity.this, "GASIKARA", "Votre compte a été créé sous le pseudo : " + response.body().getUserName());
                        RegisterActivity.this.goToLoginActivity(null);
                    }else {
                        Gson gson = new GsonBuilder().create();
                        LoginResponse errResponse = gson.fromJson(response.errorBody().string(), LoginResponse.class);
                        for (Map.Entry<String, String> entry : errResponse.getErrors().entrySet()) {
                            String key = entry.getKey();
                            String value = entry.getValue();
                            Toast.makeText(RegisterActivity.this, key + ": " + value, Toast.LENGTH_LONG).show();
                        }
                    }
                } catch (IOException e) {
                    Toast.makeText(RegisterActivity.this, "Login FAILED", Toast.LENGTH_LONG).show();
                    throw new RuntimeException(e);
                }finally {
                    hideLoadingSpinner();
                }

            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                Toast.makeText(RegisterActivity.this, "Throwable"+t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                hideLoadingSpinner();
            }
        });
    }

    public void goToLoginActivity(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void showLoadingSpinner() {
        progressBar.setVisibility(View.VISIBLE);
        BTNRegister.setVisibility(View.GONE);
    }

    private void hideLoadingSpinner() {
        progressBar.setVisibility(View.GONE);
        BTNRegister.setVisibility(View.VISIBLE);
    }
}