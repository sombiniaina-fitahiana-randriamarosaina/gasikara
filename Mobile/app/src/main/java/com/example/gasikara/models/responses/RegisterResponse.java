package com.example.gasikara.models.responses;

import com.example.gasikara.tools.Response;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse extends Response {
    @SerializedName("Data")
    private Data data;


    public  String getUserName(){
        return this.data.getUsername();
    }
    public class Data {
        @SerializedName("pseudo")
        private String username;

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }
}
