package com.example.gasikara.tools;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Response {
    @SerializedName("Meta")
    protected Meta meta;

    public int getStatus() {
        return meta.getStatus();
    }

    public void setStatus(int status) {
        meta.setStatus(status);
    }

    public String getMessage() {
        return meta.getMessage();
    }

    public void setMessage(String message) {
        meta.setMessage(message);
    }

    public Map<String, String> getErrors(){
        return meta.getErrors();
    }
}
