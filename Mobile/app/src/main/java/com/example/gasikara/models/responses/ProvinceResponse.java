package com.example.gasikara.models.responses;

import com.example.gasikara.models.Province;
import com.example.gasikara.tools.Meta;
import com.google.gson.annotations.SerializedName;

public class ProvinceResponse extends Meta {
    @SerializedName("Data")
    private Province data;
    public Province getData() {
        return data;
    }
    public void setData(Province data) {
        this.data = data;
    }
}
