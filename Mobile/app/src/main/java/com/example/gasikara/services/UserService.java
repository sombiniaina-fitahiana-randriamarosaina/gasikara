package com.example.gasikara.services;

import com.example.gasikara.models.payloads.LoginPayload;
import com.example.gasikara.models.responses.LoginResponse;
import com.example.gasikara.models.payloads.RegisterPayload;
import com.example.gasikara.models.responses.RegisterResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface UserService {
    @POST("login/")
    Call<LoginResponse> userLogin(@Body LoginPayload payload);

    @POST("register/")
    Call<RegisterResponse> userRegister(@Body RegisterPayload payload);
}
