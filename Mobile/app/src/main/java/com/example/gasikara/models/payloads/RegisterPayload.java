package com.example.gasikara.models.payloads;

import com.google.gson.annotations.SerializedName;

public class RegisterPayload {
    public RegisterPayload(String username, String password, String passwordConfirm) {
        this.setUsername(username);
        this.setPassword(password);
        this.setPasswordConfirm(passwordConfirm);
    }

    @SerializedName("pseudo")
    private String username;

    @SerializedName("password")
    private String password;

    @SerializedName("passwordConfirm")
    private String passwordConfirm;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String password) {
        this.passwordConfirm = password;
    }
}
