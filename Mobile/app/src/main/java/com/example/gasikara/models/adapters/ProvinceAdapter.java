package com.example.gasikara.models.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gasikara.R;
import com.example.gasikara.models.Province;

import java.util.List;

public class ProvinceAdapter extends RecyclerView.Adapter<ProvinceAdapter.ProvinceViewHolder> {

    private Context context;
    private List<Province> provinceList;

    private OnItemClickListener onItemClickListener;


    public ProvinceAdapter(Context context, List<Province> provinceList) {
        this.context = context;
        this.provinceList = provinceList;
    }

    @NonNull
    @Override
    public ProvinceViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.activity_province_layout, parent, false);
        return new ProvinceViewHolder(view);
    }


    public String truncateString(String input, int maxLength) {
        if (input.length() <= maxLength) {
            return input;
        } else {
            return input.substring(0, maxLength) + "...";
        }
    }

    @Override
    public void onBindViewHolder(@NonNull ProvinceViewHolder holder, int position) {
        Province province = provinceList.get(position);
        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ProvinceAdapter.this.onItemClickListener.onItemClick(province.getId());
            }
        });
        //holder.provinceImageView.setImageResource(province.getImageResourceId());
        holder.provinceNameTextView.setText(province.getName());
        holder.provinceDescription.setText(this.truncateString(province.getDescription(), 150));
    }

    @Override
    public int getItemCount() {
        return provinceList.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.onItemClickListener = listener;
    }

    public static class ProvinceViewHolder extends RecyclerView.ViewHolder {

        ImageView provinceImageView;
        TextView provinceNameTextView;

        TextView provinceDescription;

        public ProvinceViewHolder(@NonNull View itemView) {
            super(itemView);
            //provinceImageView = itemView.findViewById(R.id.provinceImageView);
            provinceNameTextView = itemView.findViewById(R.id.provinceNameTextView);
            provinceDescription = itemView.findViewById(R.id.provinceDescription);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(int id);
    }
}
