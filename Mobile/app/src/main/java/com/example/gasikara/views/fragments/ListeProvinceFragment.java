package com.example.gasikara.views.fragments;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.gasikara.R;
import com.example.gasikara.models.Province;
import com.example.gasikara.models.adapters.ProvinceAdapter;
import com.example.gasikara.models.responses.ProvinceResponse;
import com.example.gasikara.models.responses.ProvincesResponse;
import com.example.gasikara.services.ApiClient;
import com.example.gasikara.tools.Token;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListeProvinceFragment extends Fragment implements ProvinceAdapter.OnItemClickListener {

    private RecyclerView listProvincesRecyclerView;

    // Detail
    private Button backButton;
    private ConstraintLayout detailProvince;
    private TextView provinceSelectedName;
    private TextView provinceSelectedDescription;

    private TextView provinceSelectedArea;

    private VideoView videoView;

    private ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list_provinces, container, false);
        listProvincesRecyclerView = view.findViewById(R.id.recyclerView);

        // Detail
        backButton = view.findViewById(R.id.backButton);
        detailProvince = view.findViewById(R.id.detailProvince);
        provinceSelectedName = view.findViewById(R.id.provinceName);
        provinceSelectedDescription = view.findViewById(R.id.provinceDescription);
        provinceSelectedArea = view.findViewById(R.id.provinceArea);
        videoView = view.findViewById(R.id.videoView);

        progressBar = view.findViewById(R.id.progressBar);

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListeProvinceFragment.this.showProvincesList(view);
            }
        });

        // Video
        String videoPath = "android.resource://" + getContext().getPackageName() + "/" + R.raw.video;
        videoView.setVideoURI(Uri.parse(videoPath));

        // Ajouter un écouteur pour redémarrer la vidéo quand elle se termine
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                videoView.start();
            }
        });

        // Démarrer la lecture de la vidéo
        videoView.start();

        this.showProvincesList(view);

        return view;
    }

    private void showProvincesList(View view) {
        showLoadingSpinner(listProvincesRecyclerView); // // Masquer les autres View et afficher la View Spinner

        String authToken = Token.getToken(getContext());

        RecyclerView recyclerView = view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));

        Call<ProvincesResponse> call = ApiClient.getInstance(getContext()).getProvinceService().getProvinces("Bearer " + authToken);
        call.enqueue(new Callback<ProvincesResponse>() {
            @Override
            public void onResponse(Call<ProvincesResponse> call, Response<ProvincesResponse> response) {
                if (response.isSuccessful()) {
                    ProvincesResponse provinceResponse = response.body();
                    if (provinceResponse != null) {
                        List<Province> provinces = provinceResponse.getData();
                        if (provinces != null) {
                            ProvinceAdapter provinceAdapter = new ProvinceAdapter(view.getContext(), provinces);
                            provinceAdapter.setOnItemClickListener(ListeProvinceFragment.this);
                            recyclerView.setAdapter(provinceAdapter);
                            hideLoadingSpinner(listProvincesRecyclerView);
                        }
                    } else {
                        Toast.makeText(getContext(), response.code(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    Toast.makeText(getContext(), "Échec de la requête. Code de statut : " + response.code(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ProvincesResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onItemClick(int id) {
        showLoadingSpinner(detailProvince); // Masquer les autres View et afficher la View Spinner

        String authToken = Token.getToken(getContext());
        Call<ProvinceResponse> call = ApiClient.getInstance(getContext()).getProvinceService().getProvinceById("Bearer " + authToken, id);

        call.enqueue(new Callback<ProvinceResponse>() {
            @Override
            public void onResponse(Call<ProvinceResponse> call, Response<ProvinceResponse> response) {
                if (response.isSuccessful()) {
                    ProvinceResponse provinceResponse = response.body();
                    if (provinceResponse != null) {
                        Province province = provinceResponse.getData();
                        provinceSelectedName.setText(province.getName());
                        provinceSelectedDescription.setText(province.getDescription().replace(". ", ".\n\n"));
                        provinceSelectedArea.setText(province.getArea());
                    }
                } else {
                    Toast.makeText(getContext(), "Échec de la requête. Code de statut : " + response.code(), Toast.LENGTH_LONG).show();
                }
                hideLoadingSpinner(detailProvince);
            }

            @Override
            public void onFailure(Call<ProvinceResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                hideLoadingSpinner(detailProvince);
            }
        });
    }
    private void showLoadingSpinner(View view) {
        progressBar.setVisibility(View.VISIBLE);
        listProvincesRecyclerView.setVisibility(View.GONE);
        detailProvince.setVisibility(View.GONE);
        view.setVisibility(View.GONE);
    }

    private void hideLoadingSpinner(View view) {
        progressBar.setVisibility(View.GONE);
        listProvincesRecyclerView.setVisibility(View.GONE);
        detailProvince.setVisibility(View.GONE);
        view.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // Libérer les ressources du VideoView lorsque l'activité est détruite
        if (videoView != null) {
            videoView.stopPlayback();
            videoView = null;
        }
    }
}