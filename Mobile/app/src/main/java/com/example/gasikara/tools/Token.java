package com.example.gasikara.tools;

import android.content.Context;
import android.content.SharedPreferences;

public class Token {
    public static void saveToken(Context context, String token) {
        if (token != null) {
            // Stockez le token et la date d'expiration
            SharedPreferences.Editor editor = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE).edit();
            editor.putString("token", token);
            editor.apply();
        }
    }

    public static String getToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE);
        return prefs.getString("token", "");
    }

    public static void removeToken(Context context) {
        SharedPreferences.Editor editor = context.getSharedPreferences("MyPrefs", Context.MODE_PRIVATE).edit();
        editor.remove("token");
        editor.apply();
    }
}
