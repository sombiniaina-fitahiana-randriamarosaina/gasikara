package com.example.gasikara.models;

import android.content.Context;
import android.content.SharedPreferences;

public class User {
    //proprities
    private String username;
    private String password;
    private  String accessToken;

    public User() {
    }

    public User(String email, String password, String accessToken) {
        this.username = email;
        this.password = password;
        this.accessToken = accessToken;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


}
