package com.example.gasikara.views;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.widget.ImageView;

import com.example.gasikara.R;
import com.example.gasikara.tools.Token;

public class HomeActivity extends BaseNoActionBarActivity {

    // Fields
    private Handler handler;
    private ImageView img;

    @SuppressLint("Range")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        int delay = getResources().getInteger(R.integer.delay_splash_screen);

        handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                String token = Token.getToken(HomeActivity.this);
                Class activity = LoginActivity.class;

                if(token != null && !TextUtils.isEmpty(token.trim())){
                    activity = MainActivity.class;
                }

                Intent dsp = new Intent(HomeActivity.this, activity);
                startActivity(dsp);
                finish();
            }
        }, delay);
    }
}