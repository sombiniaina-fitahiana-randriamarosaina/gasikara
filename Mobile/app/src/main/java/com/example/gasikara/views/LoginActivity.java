package com.example.gasikara.views;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.gasikara.R;
import com.example.gasikara.models.payloads.LoginPayload;
import com.example.gasikara.models.responses.LoginResponse;
import com.example.gasikara.services.ApiClient;
import com.example.gasikara.tools.Token;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends BaseNoActionBarActivity{
    private EditText usernameValue, passwordValue;
    private Button buttonLogin;

    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressBar = findViewById(R.id.progressBar);

        usernameValue = findViewById(R.id.username);
        passwordValue = findViewById(R.id.password);
        buttonLogin = findViewById(R.id.buttonLogin);

        // Définir le login et mot de passe par défaut dans les zones de saisie
        String defaultUsername = "Rakoto";
        String defaultPassword = "123456";

        usernameValue.setText(defaultUsername);
        passwordValue.setText(defaultPassword);


        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLoadingSpinner();
                if (TextUtils.isEmpty(usernameValue.getText().toString()) || TextUtils.isEmpty(passwordValue.getText().toString())){
                    Toast.makeText(LoginActivity.this, "Username / Password Required", Toast.LENGTH_LONG).show();
                }else {
                    login();
               }
            }
        });
    }

    public void login(){
        LoginPayload loginPayload = new LoginPayload(usernameValue.getText().toString(), passwordValue.getText().toString());
        Call<LoginResponse> loginResponseCall = ApiClient.getInstance(getApplicationContext()).getUserService().userLogin(loginPayload);
        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                try {
                    if (response.isSuccessful()){
                        LoginResponse loginResponse = response.body();
                        Toast.makeText(LoginActivity.this, "Connexion réussie", Toast.LENGTH_LONG).show();

                        Token.saveToken(LoginActivity.this, loginResponse.getToken());

                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();

                    }else {
                        Gson gson = new GsonBuilder().create();
                        LoginResponse errResponse = gson.fromJson(response.errorBody().string(), LoginResponse.class);

                        Toast.makeText(LoginActivity.this, errResponse.getMessage(), Toast.LENGTH_LONG).show();
                    }
                } catch (IOException e) {
                    Toast.makeText(LoginActivity.this, "Login FAILED", Toast.LENGTH_LONG).show();
                    throw new RuntimeException(e);
                }finally {
                    hideLoadingSpinner();
                }

            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(LoginActivity.this, "Throwable"+t.getLocalizedMessage(), Toast.LENGTH_LONG).show();
                hideLoadingSpinner();
            }
        });
    }


    public void goToRegisterActivity(View view) {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    private void showLoadingSpinner() {
        progressBar.setVisibility(View.VISIBLE);
        buttonLogin.setVisibility(View.GONE);
    }

    private void hideLoadingSpinner() {
        progressBar.setVisibility(View.GONE);
        buttonLogin.setVisibility(View.VISIBLE);
    }

}

