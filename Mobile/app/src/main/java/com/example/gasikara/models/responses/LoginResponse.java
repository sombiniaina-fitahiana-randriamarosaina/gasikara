package com.example.gasikara.models.responses;

import com.example.gasikara.tools.Response;
import com.google.gson.annotations.SerializedName;

public class LoginResponse extends Response {
    @SerializedName("Data")
    private Data data;

    public String getToken(){
        return data.getToken();
    }
    public class Data {
        @SerializedName("token")
        private String token;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

    }
}

