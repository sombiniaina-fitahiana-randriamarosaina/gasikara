package com.example.gasikara.views;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import androidx.viewpager2.widget.ViewPager2;

import com.example.gasikara.R;
import com.example.gasikara.models.BottomBarItem;
import com.example.gasikara.views.fragments.HomeFragment;
import com.example.gasikara.views.fragments.ListeProvinceFragment;
import com.example.gasikara.views.fragments.SettingsFragment;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends BaseNoActionBarActivity {
    TabLayout tabLayout;
    ViewPager2 viewPager2;
    MyViewPagerAdapter myViewPagerAdapter;
    private int selectedTab = 1;

    private List<BottomBarItem> BottomBarItems;

    protected void init(){
        BottomBarItems = new ArrayList<>();

        BottomBarItem home = new BottomBarItem(HomeFragment.class, findViewById(R.id.homeLayout), findViewById(R.id.homeImage), findViewById(R.id.homeTxt));
        BottomBarItem locations = new BottomBarItem(ListeProvinceFragment.class, findViewById(R.id.locationLayout), findViewById(R.id.locationImage), findViewById(R.id.locationTxt));
        BottomBarItem settings = new BottomBarItem(SettingsFragment.class, findViewById(R.id.settingsLayout), findViewById(R.id.settingsImage), findViewById(R.id.settingsTxt));

        BottomBarItems.add(home);
        BottomBarItems.add(locations);
        BottomBarItems.add(settings);
    }

    protected void setActive(Class type){
        for (BottomBarItem item: BottomBarItems) {
            if(item.getClassFragment() == type){
                getSupportFragmentManager()
                        .beginTransaction()
                        .setReorderingAllowed(true)
                        .replace(R.id.fragmentContainer, type,null)
                        .commit();
                item.show();

            }else{
                item.hidden();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();

        final LinearLayout homeLayout=findViewById(R.id.homeLayout);
        final LinearLayout locationLayout=findViewById(R.id.locationLayout);
        final LinearLayout settingsLayout=findViewById(R.id.settingsLayout);

        MainActivity.this.setActive(HomeFragment.class);
        selectedTab = 1;

        homeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedTab != 1){
                    MainActivity.this.setActive(HomeFragment.class);
                    selectedTab = 1;
                }
            }
        });
        locationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedTab != 2){
                    MainActivity.this.setActive(ListeProvinceFragment.class);
                    selectedTab = 2;
                }
            }
        });
        settingsLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (selectedTab != 3){
                    MainActivity.this.setActive(SettingsFragment.class);
                    selectedTab = 3;
                }
            }
        });
    }
}