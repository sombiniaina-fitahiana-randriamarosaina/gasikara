package com.example.gasikara.services;

import android.content.Context;

import com.example.gasikara.R;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private ApiClient(Context context){
        this.context = context;
    }
    private static ApiClient instance = null;
    private static Context context;
    private static Retrofit retrofit;

    public static ApiClient getInstance(Context context){
        if(instance == null) instance = new ApiClient(context);
        return instance;
    }
    public Retrofit getRetrofit() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(context.getResources().getString(R.string.base_url))
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public ProvinceService getProvinceService() {
        return getRetrofit().create(ProvinceService.class);
    }

    public UserService getUserService(){
        UserService userService = getRetrofit().create(UserService.class);
        return userService;
    }
}
