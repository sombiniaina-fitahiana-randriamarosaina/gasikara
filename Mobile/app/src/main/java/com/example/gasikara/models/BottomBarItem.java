package com.example.gasikara.models;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class BottomBarItem {
    public BottomBarItem(Class classFragment, LinearLayout layout, ImageView image, TextView text){
        this.classFragment = classFragment;
        this.layout = layout;
        this.image = image;
        this.text = text;
    }

    private Class classFragment;
    private LinearLayout layout;
    private ImageView image;
    private TextView text;

    public Class getClassFragment() {
        return classFragment;
    }

    public void hidden(){
        this.text.setVisibility(View.GONE);
        //this.layout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
    }

    public void show(){
        this.text.setVisibility(View.VISIBLE);
        ScaleAnimation scaleAnimation = new ScaleAnimation( 0.8f, 1.0f, 1f, 1f, Animation.RELATIVE_TO_SELF,0.0f,Animation.RELATIVE_TO_SELF, 0.0f);
        scaleAnimation.setDuration(200);
        scaleAnimation.setFillAfter(true);
        this.layout.startAnimation(scaleAnimation);
    }
}
