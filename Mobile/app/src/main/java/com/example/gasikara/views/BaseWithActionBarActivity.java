package com.example.gasikara.views;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.appcompat.app.AppCompatActivity;
//import androidx.preference.PreferenceManager;

import com.example.gasikara.R;

public class BaseWithActionBarActivity extends AppCompatActivity  {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Récupérer le thème actuel depuis les préférences partagées
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String selectedTheme = sharedPreferences.getString("pref_key_theme", "AppTheme.Light");
        setTheme(selectedTheme.equals("AppTheme.Dark") ? R.style.AppTheme_Dark : R.style.AppTheme_Light);

        super.onCreate(savedInstanceState);
    }
}
