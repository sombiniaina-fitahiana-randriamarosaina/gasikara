package com.example.gasikara.models.adapters;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.core.content.ContextCompat;
import androidx.viewpager.widget.PagerAdapter;

import com.example.gasikara.R;

public class ImagePagerAdapter extends PagerAdapter {
    private Context context;
    private int[] imageIds;

    public ImagePagerAdapter(Context context, int[] imageIds) {
        this.context = context;
        this.imageIds = imageIds;
        this.renderScript = RenderScript.create(context);
        this.blurScript = ScriptIntrinsicBlur.create(renderScript, Element.U8_4(renderScript));
    }

    private RenderScript renderScript;
    private ScriptIntrinsicBlur blurScript;

    @Override
    public int getCount() {
        return imageIds.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_image, container, false);
        ImageView imageView = view.findViewById(R.id.imageView);

        Drawable blurredDrawable = getBlurredDrawable(imageIds[position]);
        if (blurredDrawable != null) {
            imageView.setImageDrawable(blurredDrawable);
        } else {
            imageView.setImageResource(imageIds[position]);
        }


        container.addView(view);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    public Drawable getBlurredDrawable(int id) {
        int radius = 5; // Ajuster le rayon de flou selon vos préférences
        Drawable drawable = ContextCompat.getDrawable(context, id);
        BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;

        if (bitmapDrawable != null) {
            Bitmap bitmap = bitmapDrawable.getBitmap();
            Bitmap blurredBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);

            Allocation allocationIn = Allocation.createFromBitmap(renderScript, bitmap);
            Allocation allocationOut = Allocation.createFromBitmap(renderScript, blurredBitmap);

            blurScript.setRadius(radius);
            blurScript.setInput(allocationIn);
            blurScript.forEach(allocationOut);

            allocationOut.copyTo(blurredBitmap);
            return new BitmapDrawable(context.getResources(), blurredBitmap);
        }

        return null;
    }
}
