package com.example.gasikara.models.payloads;

import com.google.gson.annotations.SerializedName;

public class LoginPayload {
    @SerializedName("pseudo")
    private String username;

    @SerializedName("password")
    private String password;

    public LoginPayload(String username, String password) {
        this.setUsername(username);
        this.setPassword(password);
    }

    public LoginPayload() {

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
