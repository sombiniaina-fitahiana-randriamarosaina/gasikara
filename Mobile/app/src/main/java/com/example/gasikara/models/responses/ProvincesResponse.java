package com.example.gasikara.models.responses;

import com.example.gasikara.models.Province;
import com.example.gasikara.tools.Meta;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProvincesResponse extends Meta {
    @SerializedName("Data")
    private List<Province> data;
    public List<Province> getData() {
        return data;
    }
    public void setData(List<Province> data) {
        this.data = data;
    }
}
