package com.example.gasikara.services;

import com.example.gasikara.models.responses.ProvinceResponse;
import com.example.gasikara.models.responses.ProvincesResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface ProvinceService {
    @GET("provinces")
    Call<ProvincesResponse> getProvinces(@Header("Authorization") String token);

    @GET("provinces/{id}")
    Call<ProvinceResponse> getProvinceById(@Header("Authorization") String token, @Path("id")int id);
}
