package com.example.gasikara.tools;

import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Meta {
    @SerializedName("status")
    private int status;
    @SerializedName("message")
    private String message;

    @SerializedName("errors")
    private Map<String, String> errors;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, String> getErrors(){
        return this.errors;
    }

    public void setErrors(Map<String, String> errors){
        this.errors = errors ;
    }
}
