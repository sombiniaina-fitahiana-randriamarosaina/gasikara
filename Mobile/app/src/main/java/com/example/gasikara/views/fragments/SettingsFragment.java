package com.example.gasikara.views.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import com.example.gasikara.R;
import com.example.gasikara.tools.Token;
import com.example.gasikara.views.LoginActivity;

public class SettingsFragment extends PreferenceFragmentCompat {
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.preferences, rootKey);


        Preference buttonPreference = findPreference("button_logout");
        buttonPreference.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
            @Override
            public boolean onPreferenceClick(Preference preference) {
                Token.removeToken(getContext());
                Intent dsp = new Intent(getContext(), LoginActivity.class);
                startActivity(dsp);
                getActivity().finish();
                return true;
            }
        });

        // Lorsque la préférence du thème change, mettre à jour le thème de l'application
        ListPreference themePreference = findPreference("pref_key_theme");
        if (themePreference != null) {
            themePreference.setSummaryProvider(ListPreference.SimpleSummaryProvider.getInstance());

            themePreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object newValue) {
                    String newTheme = (String) newValue;
                    getActivity().recreate(); // Redémarrer l'activité pour appliquer le nouveau thème
                    return true;
                }
            });
        }
    }
}
