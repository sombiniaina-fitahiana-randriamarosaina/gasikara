package com.example.gasikara.views.fragments;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.example.gasikara.R;
import com.example.gasikara.models.adapters.ImagePagerAdapter;

public class HomeFragment extends Fragment {
    private TextView tappingText;
    private int charIndex = 0;
    private long delay = 100; // Délai entre chaque caractère (en millisecondes)

    private int textIndex = 0;

    private String[] texts = {
            "Tongasoa !!",
            "Bienvenue !!",
            "Mbala tsara e!!",
            "Welcome !!",
            "Salanitra ee!",
            "Bonjour !! ",
            "Akory ee!!",
            "Hello !!"
    };

    private boolean isTaping;

    private int[] imageIds = {
            R.drawable.background_1,
            R.drawable.background_2,
            R.drawable.background_3,
            R.drawable.background_4,
            R.drawable.background_5,
            R.drawable.background_6,
            R.drawable.background_7,
            R.drawable.background_8
    };

    private ViewPager viewPager;
    private ImagePagerAdapter adapter;
    private int currentPage = 0;
    private boolean isAutoScroll = true;
    private final long autoScrollDelay = 5000; // Définir l'intervalle de défilement automatique (en millisecondes)

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        viewPager = view.findViewById(R.id.viewPager);
        adapter = new ImagePagerAdapter(view.getContext(), imageIds);
        viewPager.setAdapter(adapter);

        // Ajouter le défilement automatique
        autoScroll();

        // Inflate the layout for this fragment
        tappingText = view.findViewById(R.id.tappingText);
        this.isTaping = true;
        typingAnimation();
        return view;
    }

    private void autoScroll() {
        final Handler handler = new Handler();
        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                if (isAutoScroll) {
                    currentPage = (currentPage + 1) % imageIds.length;
                    viewPager.setCurrentItem(currentPage);
                    handler.postDelayed(this, autoScrollDelay);
                }
            }
        };
        handler.postDelayed(runnable, autoScrollDelay);
    }

    private void typingAnimation() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String text = texts[textIndex];
                if(isTaping){
                    tappingText.setText(text.substring(0, charIndex++));
                }else {
                    tappingText.setText(text.substring(0, charIndex--));
                }
                typingAnimation();
            }
        }, delay);

        if(charIndex == texts[textIndex].length()) {
            isTaping = false;
            delay = 1000;
        }
        else if(charIndex == 0) {
            isTaping = true;
            delay = 1000;
            textIndex = (textIndex == texts.length - 1) ? 0 : textIndex + 1;
        }else {
            delay = 100;
        }
    }
}