CREATE TABLE Users(
    ID SERIAL PRIMARY KEY,
    NAME VARCHAR(255) NOT NULL,
    PASSWORD VARCHAR(40) NOT NULL
);


INSERT INTO Users (NAME, PASSWORD) VALUES ('Rakoto', '7c4a8d09ca3762af61e59520943dc26494f8941b');


CREATE TABLE Provinces(
    IDPROVINCE SERIAL PRIMARY KEY,
    NAME VARCHAR(255) NOT NULL,
    DESCRIPTION TEXT NOT NULL,
    AREA VARCHAR(255)
);
INSERT INTO Provinces(NAME, DESCRIPTION, AREA) VALUES
('Antananarivo', 'La province d''Antananarivo, située au centre de Madagascar, est dominée par la capitale du pays, Antananarivo. Elle offre un mélange unique de modernité et de traditions malgaches. Les visiteurs peuvent explorer des monuments historiques tels que le palais de la Reine et profiter de la vue panoramique depuis les collines environnantes. La région est riche en parcs et jardins, où les amoureux de la nature pourront se détendre et se promener.', 'Environ 58 283 km²'),
('Antsiranana ', 'La province d''Antsiranana, au nord de Madagascar, est une destination touristique incontournable. Elle est réputée pour sa beauté naturelle époustouflante, avec des baies majestueuses, des montagnes imposantes, et des tsingy uniques. Les amateurs de randonnée et d''exploration seront enchantés par les parcs nationaux comme Montagne d''Ambre et l''Ankarana. La ville portuaire de Diego Suarez offre une ambiance animée avec ses marchés colorés et ses restaurants conviviaux.', 'Environ 43 046 km²'),
('Fianarantsoa ', 'La province de Fianarantsoa, située dans le sud de Madagascar, est un véritable paradis pour les amoureux de la nature et de la culture. Les paysages sont diversifiés, allant des montagnes luxuriantes aux vastes étendues de savanes. Les visiteurs peuvent explorer les parcs nationaux de Ranomafana et d''Andringitra, ainsi que les plantations de thé de Sahambavy. La ville de Fianarantsoa est célèbre pour ses traditions Betsileo et ses danses folkloriques envoûtantes.', 'Environ 103 272 km²'),
('Mahajanga ', 'La province de Mahajanga, dans le nord-ouest de Madagascar, est une destination balnéaire prisée. Les plages immaculées et les eaux turquoise attirent les voyageurs en quête de détente. Les parcs nationaux comme Ankarafantsika offrent une opportunité d''observer une faune et une flore variées. La ville de Mahajanga propose une atmosphère animée avec ses marchés colorés, ses restaurants et ses rues animées.', 'Environ 152 033 km²'),
('Toamasina ', 'La province de Toamasina, à l''est de Madagascar, est une région riche en biodiversité et en beauté naturelle. Les visiteurs pourront explorer les parcs nationaux de Zahamena et d''Andasibe, où ils auront la chance d''observer des lémuriens et des espèces endémiques. La ville portuaire de Toamasina offre un mélange de cultures, avec une histoire influencée par les colons français et malgaches.', 'Environ 71 911 km²'),
('Toliara', 'La province de Toliara, dans le sud-ouest de Madagascar, est une région aux paysages variés allant des savanes arides aux formations rocheuses spectaculaires. Les visiteurs peuvent explorer le parc national de l''Isalo, avec ses canyons et ses piscines naturelles, ainsi que la réserve de Tsimanampetsotsa avec sa faune et sa flore uniques. La région offre également de magnifiques plages, notamment à Ifaty, idéales pour la plongée et la détente.', 'Environ 161 405 km²')