export default class Exception extends Error{
    private datas : {[key : string] : any};

    constructor(message : string){
        super(message);
        this.datas = {};
    }

    public add(key : string, value : any) : void{
        this.datas[key] = value;
    }

    public getDatas() : {[key : string] : any}{
        return this.datas;
    }

    public static throwsException(key : string, value : any){
        const e : Exception = new Exception("");
        e.add(key, value);
        throw e;
    }
}