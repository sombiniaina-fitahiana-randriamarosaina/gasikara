export interface HttpResponse<T> {
    Meta: {
        status: number;
        message: string;
        errors: object | null;
    };
    Data: T | null;
}

  
export class HttpApiResponse<T> {
    private meta: {
        status: number;
        message: string;
        errors: object | null;
    };
  
    private data: T | null;
  
    constructor(data: T | null, status: number = 200, message: string = "Ok", errors: object | null = null) {
        this.meta = {
            status,
            message,
            errors
        };
        this.data = data;
    }
  
    public getResponse(): HttpResponse<T> {
        return {
            Meta: this.meta,
            Data: this.data,
        };
    }
}
  