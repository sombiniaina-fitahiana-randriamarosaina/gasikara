import { Request, Response , NextFunction} from 'express';
import jwt from 'jsonwebtoken';
import { HttpApiResponse } from './HttpResponse';

export default class Token {
    private static JWT_SECRET_KEY = "GASIKARA_SCRET_KEY";
    private static JWT_EXPIRATION = "24h";

    public static control (req: Request, res: Response, next : NextFunction) {
        const {token} = req;
        jwt.verify(token ?? "", Token.JWT_SECRET_KEY, (err : any, decoded : any) => {
            if (err) {
                const response = new HttpApiResponse({token}, 403, err.message);
                res.status(403).json(response.getResponse());
            } else if(Date.now() >= decoded.exp * 1000){
                const response = new HttpApiResponse({token}, 403, "Token Expired");
                res.status(403).json(response.getResponse());
            }else{
                next();
            }
        });
    }

    public static generate(object : object) : string{
        return jwt.sign(object, Token.JWT_SECRET_KEY, { expiresIn: Token.JWT_EXPIRATION });
    }
}