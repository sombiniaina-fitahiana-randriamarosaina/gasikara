import {SHA1} from 'crypto-js';
import { Pool } from 'pg';
import Exception from '../tools/Exception';

export default class Provinces {
    constructor(id : string | undefined, name : string | undefined, description : string | undefined, area : string | undefined){
        this.setId(id);
        this.setName(name);
        this.setDescription(description);
        this.setArea(area);
    }

    private id : string | undefined;
    private name : string | undefined;
    private description : string | undefined;
    private area : string | undefined;

    public setId(id : string | undefined){
        this.id = id;
    }

    public getId() : string | undefined {
        return this.id;
    }

    public setName(name : string | undefined){
        this.name = name;
    }
    
    public getName() : string | undefined {
        return this.name;
    }

    public setDescription(description : string | undefined){
        this.description = description;
    }
    
    public getDescription() : string | undefined {
        return this.description;
    }

    public setArea(area : string | undefined){
        this.area = area;
    }
    
    public getArea() : string | undefined {
        return this.area;
    }

    public static async findAll(pool : Pool) : Promise<Provinces[]>{
        let values : Array<Provinces> = [];
        const result = await pool.query("SELECT * FROM Provinces");
        if(result.rows.length !== 0) {
            result.rows.forEach(province => {
                values.push(new Provinces(province.idprovince, province.name, province.description, province.area)); 
            });
        }
        return values;
    }

    public static async findById(pool : Pool, id : string) : Promise<Provinces|undefined>{
        const query = {
            text: 'SELECT * FROM Provinces WHERE IDPROVINCE = $1',
            values: [id],
        };
        const result = await pool.query(query);
        if(result.rows.length !== 0) {
            let province = result.rows[0];
            return new Provinces(province.idprovince, province.name, province.description, province.area); 
        }else{
            return undefined;
        }
    }
}