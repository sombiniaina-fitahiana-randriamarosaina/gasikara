import {SHA1} from 'crypto-js';
import { Pool } from 'pg';
import Exception from '../tools/Exception';

export default class Users {
    constructor(id : string | undefined, name : string | undefined, password : string | undefined){
        this.setId(id);
        this.setName(name);
        this.setPassword(password);
    }

    private id : string | undefined;
    private name : string | undefined;
    private password : string | undefined;

    public setId(id : string | undefined){
        this.id = id;
    }

    public getId() : string | undefined {
        return this.id;
    }

    public setName(name : string | undefined){
        this.name = name;
    }
    
    public getName() : string | undefined {
        return this.name;
    }

    public setPassword(password : string | undefined){
        this.password = password;
    }
    
    public getPassword() : string | undefined {
        return this.password;
    }

    public async login(pool : Pool) {
        const query = {
            text: 'SELECT * FROM Users WHERE NAME = $1',
            values: [this.getName()],
        };
        const result = await pool.query(query);
        if(result.rows.length === 0) Exception.throwsException("pseudo", "Utilisateur non trouvé !!");
        else if(result.rows[0].password !== SHA1(this.getPassword() ?? "").toString()) Exception.throwsException("password" , "Mot de passe incorrect !!");

        this.setId(result.rows[0].id);
    }

    public async register(pool : Pool){
        // Controle
        if(!this.getName() || this.getName()?.trim().length == 0) Exception.throwsException("pseudo", "Le champ pseudo doit être renseigné");
        if(!this.getPassword() || this.getPassword()?.trim().length == 0) Exception.throwsException("password", "Le champ pseudo doit être renseigné");
        const serchQuery = {
            text: "SELECT * FROM Users WHERE NAME LIKE $1",
            values: [`%${this.getName()}%`],
        };
        const resultSearch = await pool.query(serchQuery);
        if(resultSearch.rows.length > 0) Exception.throwsException("pseudo", "Pseudo déjà utilisé");

        // Insertion
        const query = {
            text: 'INSERT INTO USERS (NAME, PASSWORD) VALUES ($1, $2) RETURNING id',
            values: [this.getName(), SHA1(this.getPassword() ?? "").toString()]
        };
        const result = await pool.query(query);
        this.setId(result.rows[0].id);
    }
}