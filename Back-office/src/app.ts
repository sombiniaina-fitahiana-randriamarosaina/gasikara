import cors from 'cors';
import express, { Request, Response , NextFunction} from 'express';
import staticRoutes from './routes/StaticRoute';
import prodRoutes from './routes/ProdRoute';


const app = express();

app.use(cors()); // CORS

/*************** Extract Bearear token************************/
app.use((req: Request, res: Response, next : NextFunction) => {
    const bearerHeader  = req.headers['authorization'];
    if(typeof bearerHeader != 'undefined' && bearerHeader.startsWith('Bearer')){
        const bearer = bearerHeader.split(' ');
        req.token = bearer[1];
    }
    next();
})
/*************** Extract Bearear token************************/


app.get('/', (req: Request, res: Response) => {
    res.send('Hello, world! Gasikara');
});

app.use('/api/static', staticRoutes);

app.use('/api', prodRoutes);


app.listen(3000, () => {
    console.log(`Example app listening on port 3000`)
})

export default app;
