import express, { Request, Response , NextFunction} from 'express';
import bodyParser from 'body-parser';
import jwt from 'jsonwebtoken';
import { HttpApiResponse } from '../tools/HttpResponse';

const villes = [{
    "id" : "1",
    "nom" : "Antananarivo"
},{
    "id" : "2",
    "nom" : "Fianarantsoa"
},{
    "id" : "1",
    "nom" : "Toamasina"
},{
    "id" : "1",
    "nom" : "Mahajanga"
},{
    "id" : "1",
    "nom" : "Antsiranana"
},{
    "id" : "1",
    "nom" : "Toliara"
}];

const JWT_SECRET_KEY = "votre_clé_secrète";
const JWT_EXPIRATION = "24h";

const controlToken = (req: Request, res: Response, next : NextFunction) => {
    const {token} = req;
    jwt.verify(token ?? "", JWT_SECRET_KEY, (err : any, decoded : any) => {
        if (err) {
            //const response = new HttpApiResponse({token}, 403, 'Token manquant ou invalide');
            const response = new HttpApiResponse({token}, 403, err.message);
            res.status(403).json(response.getResponse());
        } else {
            next();
        }
      });
}

const router = express.Router();

router.use(bodyParser.json()) // Ajoute les parametres de body raw json dans le champ "body"  de l'objet request.

router.post('/login', (req: Request, res: Response) => {
    const { pseudo, password } = req.body;
    if(pseudo === 'admin' && password === 'password'){
        const token = jwt.sign({ pseudo }, JWT_SECRET_KEY, { expiresIn: JWT_EXPIRATION });

        const response = new HttpApiResponse({token});

        res.json(response.getResponse());
    }else{
        const data = {pseudo,password}
        const errors = [{
            "*" : 'Identifiants invalides'
        }]
        const response = new HttpApiResponse(data, 401, 'Identifiants invalides', errors);
        res.status(401).json(response.getResponse());
    }
});

router.post('/register', (req: Request, res: Response) => {
    const { pseudo, password } = req.body;
    try{
        const response = new HttpApiResponse({pseudo}, 201, 'Utilisateur créé');
        res.status(201).json(response.getResponse());
    }catch(e){
        const data = {pseudo,password}
        const errors = [{
            "*" : 'Identifiants invalides'
        }]
        const response = new HttpApiResponse(data, 401, 'Identifiants invalides', errors);
        res.status(401).json(response.getResponse());
    }
});

router.get('/villes', controlToken , (req: Request, res: Response) => {
    try{
        const response = new HttpApiResponse(villes);
        res.json(response.getResponse());
    }catch(e){
        const response = new HttpApiResponse(null, 500, 'Internal Server Errorr');
        res.status(500).json(response.getResponse());
    }
});

router.get('/villes/:id', controlToken, (req: Request, res: Response) => {
    try{
        const {id} = req.params;
        let find = villes.filter((ville) => ville.id === id);
        if(find.length == 0){
            const response = new HttpApiResponse({id}, 400, `Impossible de trouver la ville avec l'id ${id}`);
            res.json(response.getResponse());
        }else{
            const response = new HttpApiResponse(find);
            res.json(response.getResponse());
        }
    }catch(e){
        const response = new HttpApiResponse(null, 500, 'Internal Server Errorr');
        res.status(500).json(response.getResponse());
    }
});

export default router;
