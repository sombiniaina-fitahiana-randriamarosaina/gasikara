import express, { Request, Response} from 'express';
import bodyParser from 'body-parser';
import { HttpApiResponse } from '../tools/HttpResponse';
import Users from '../models/Users';
import Exception from '../tools/Exception';
import Provinces from '../models/Provinces';
import Token from '../tools/Token';
import BDD from '../tools/BDD';
import { Pool } from 'pg';

const router = express.Router();

router.use(bodyParser.json()) // Ajoute les parametres de body raw json dans le champ "body"  de l'objet request.

router.post('/login', async (req: Request, res: Response) => {
    const pool : Pool = BDD.getPool();

    const { pseudo, password } = req.body;

    const user = new Users('', pseudo, password);
    try{
        await user.login(pool);
        const token = Token.generate({ id : user.getId(), name : user.getName()});
        const response = new HttpApiResponse({token});
        res.json(response.getResponse());
    }catch(e){
        if(e instanceof Exception){
            const data = {pseudo,password}
            const errors = [e.getDatas()];
            const response = new HttpApiResponse(data, 401, 'Identifiants invalides', errors);
            res.status(401).json(response.getResponse());
        }else{
            res.status(500).json({});
        }
    }finally{
        pool.end();
    }
    
});

router.post('/register', async (req: Request, res: Response) => {
    const { pseudo, password } = req.body;
    const pool : Pool = BDD.getPool();
    const user = new Users('', pseudo, password);
    try{
        await user.register(pool);
        const response = new HttpApiResponse({pseudo}, 201, 'Utilisateur créé');
        res.status(201).json(response.getResponse());
    }catch(e){
        if(e instanceof Exception){
            const data = {pseudo,password}
            const errors = e.getDatas();
            const response = new HttpApiResponse(data, 401, 'Identifiants invalides', errors);
            res.status(401).json(response.getResponse());
        }else{
            console.log(e);
            res.status(500).json({});
        }
    }finally{
        pool.end();
    }
});

router.get('/provinces', Token.control, async (req: Request, res: Response) => {
    const pool : Pool = BDD.getPool();
    try{
        let provinces : Provinces[] = await Provinces.findAll(pool);
        const response = new HttpApiResponse(provinces);
        res.json(response.getResponse());
    }catch(e){
        const response = new HttpApiResponse(null, 500, 'Internal Server Errorr');
        res.status(500).json(response.getResponse());
    }finally{
        pool.end();
    }
});

router.get('/provinces/:id', Token.control, async (req: Request, res: Response) => {
    const pool : Pool = BDD.getPool();
    try{
        const {id} = req.params;
        let province : Provinces | undefined = await Provinces.findById(pool, id);
        if(province === undefined){
            const response = new HttpApiResponse({id}, 400, `Impossible de trouver la ville avec l'id ${id}`);
            res.json(response.getResponse());
        }else{
            const response = new HttpApiResponse(province);
            res.json(response.getResponse());
        }
    }catch(e){
        const response = new HttpApiResponse(null, 500, 'Internal Server Errorr');
        res.status(500).json(response.getResponse());
    }finally{
        pool.end();
    }
});

export default router;
