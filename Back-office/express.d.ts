// custom.d.ts
declare namespace Express {
  interface Request {
    token?: string;
  }
}
