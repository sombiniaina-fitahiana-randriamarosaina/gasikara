"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const HttpResponse_1 = require("../tools/HttpResponse");
const villes = [{
        "id": "1",
        "nom": "Antananarivo"
    }, {
        "id": "2",
        "nom": "Fianarantsoa"
    }, {
        "id": "1",
        "nom": "Toamasina"
    }, {
        "id": "1",
        "nom": "Mahajanga"
    }, {
        "id": "1",
        "nom": "Antsiranana"
    }, {
        "id": "1",
        "nom": "Toliara"
    }];
const JWT_SECRET_KEY = "votre_clé_secrète";
const JWT_EXPIRATION = "24h";
const controlToken = (req, res, next) => {
    const { token } = req;
    jsonwebtoken_1.default.verify(token !== null && token !== void 0 ? token : "", JWT_SECRET_KEY, (err, decoded) => {
        if (err) {
            //const response = new HttpApiResponse({token}, 403, 'Token manquant ou invalide');
            const response = new HttpResponse_1.HttpApiResponse({ token }, 403, err.message);
            res.status(403).json(response.getResponse());
        }
        else {
            next();
        }
    });
};
const router = express_1.default.Router();
router.use(body_parser_1.default.json()); // Ajoute les parametres de body raw json dans le champ "body"  de l'objet request.
router.post('/login', (req, res) => {
    const { pseudo, password } = req.body;
    if (pseudo === 'admin' && password === 'password') {
        const token = jsonwebtoken_1.default.sign({ pseudo }, JWT_SECRET_KEY, { expiresIn: JWT_EXPIRATION });
        const response = new HttpResponse_1.HttpApiResponse({ token });
        res.json(response.getResponse());
    }
    else {
        const data = { pseudo, password };
        const errors = [{
                "*": 'Identifiants invalides'
            }];
        const response = new HttpResponse_1.HttpApiResponse(data, 401, 'Identifiants invalides', errors);
        res.status(401).json(response.getResponse());
    }
});
router.post('/register', (req, res) => {
    const { pseudo, password } = req.body;
    try {
        const response = new HttpResponse_1.HttpApiResponse({ pseudo }, 201, 'Utilisateur créé');
        res.status(201).json(response.getResponse());
    }
    catch (e) {
        const data = { pseudo, password };
        const errors = [{
                "*": 'Identifiants invalides'
            }];
        const response = new HttpResponse_1.HttpApiResponse(data, 401, 'Identifiants invalides', errors);
        res.status(401).json(response.getResponse());
    }
});
router.get('/villes', controlToken, (req, res) => {
    try {
        const response = new HttpResponse_1.HttpApiResponse(villes);
        res.json(response.getResponse());
    }
    catch (e) {
        const response = new HttpResponse_1.HttpApiResponse(null, 500, 'Internal Server Errorr');
        res.status(500).json(response.getResponse());
    }
});
router.get('/villes/:id', controlToken, (req, res) => {
    try {
        const { id } = req.params;
        let find = villes.filter((ville) => ville.id === id);
        if (find.length == 0) {
            const response = new HttpResponse_1.HttpApiResponse({ id }, 400, `Impossible de trouver la ville avec l'id ${id}`);
            res.json(response.getResponse());
        }
        else {
            const response = new HttpResponse_1.HttpApiResponse(find);
            res.json(response.getResponse());
        }
    }
    catch (e) {
        const response = new HttpResponse_1.HttpApiResponse(null, 500, 'Internal Server Errorr');
        res.status(500).json(response.getResponse());
    }
});
exports.default = router;
//# sourceMappingURL=StaticRoute.js.map