"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const body_parser_1 = __importDefault(require("body-parser"));
const HttpResponse_1 = require("../tools/HttpResponse");
const Users_1 = __importDefault(require("../models/Users"));
const Exception_1 = __importDefault(require("../tools/Exception"));
const Provinces_1 = __importDefault(require("../models/Provinces"));
const Token_1 = __importDefault(require("../tools/Token"));
const BDD_1 = __importDefault(require("../tools/BDD"));
const router = express_1.default.Router();
router.use(body_parser_1.default.json()); // Ajoute les parametres de body raw json dans le champ "body"  de l'objet request.
router.post('/login', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const pool = BDD_1.default.getPool();
    const { pseudo, password } = req.body;
    const user = new Users_1.default('', pseudo, password);
    try {
        yield user.login(pool);
        const token = Token_1.default.generate({ id: user.getId(), name: user.getName() });
        const response = new HttpResponse_1.HttpApiResponse({ token });
        res.json(response.getResponse());
    }
    catch (e) {
        if (e instanceof Exception_1.default) {
            const data = { pseudo, password };
            const errors = [e.getDatas()];
            const response = new HttpResponse_1.HttpApiResponse(data, 401, 'Identifiants invalides', errors);
            res.status(401).json(response.getResponse());
        }
        else {
            res.status(500).json({});
        }
    }
    finally {
        pool.end();
    }
}));
router.post('/register', (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { pseudo, password } = req.body;
    const pool = BDD_1.default.getPool();
    const user = new Users_1.default('', pseudo, password);
    try {
        yield user.register(pool);
        const response = new HttpResponse_1.HttpApiResponse({ pseudo }, 201, 'Utilisateur créé');
        res.status(201).json(response.getResponse());
    }
    catch (e) {
        if (e instanceof Exception_1.default) {
            const data = { pseudo, password };
            const errors = e.getDatas();
            const response = new HttpResponse_1.HttpApiResponse(data, 401, 'Identifiants invalides', errors);
            res.status(401).json(response.getResponse());
        }
        else {
            console.log(e);
            res.status(500).json({});
        }
    }
    finally {
        pool.end();
    }
}));
router.get('/provinces', Token_1.default.control, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const pool = BDD_1.default.getPool();
    try {
        let provinces = yield Provinces_1.default.findAll(pool);
        const response = new HttpResponse_1.HttpApiResponse(provinces);
        res.json(response.getResponse());
    }
    catch (e) {
        const response = new HttpResponse_1.HttpApiResponse(null, 500, 'Internal Server Errorr');
        res.status(500).json(response.getResponse());
    }
    finally {
        pool.end();
    }
}));
router.get('/provinces/:id', Token_1.default.control, (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const pool = BDD_1.default.getPool();
    try {
        const { id } = req.params;
        let province = yield Provinces_1.default.findById(pool, id);
        if (province === undefined) {
            const response = new HttpResponse_1.HttpApiResponse({ id }, 400, `Impossible de trouver la ville avec l'id ${id}`);
            res.json(response.getResponse());
        }
        else {
            const response = new HttpResponse_1.HttpApiResponse(province);
            res.json(response.getResponse());
        }
    }
    catch (e) {
        const response = new HttpResponse_1.HttpApiResponse(null, 500, 'Internal Server Errorr');
        res.status(500).json(response.getResponse());
    }
    finally {
        pool.end();
    }
}));
exports.default = router;
//# sourceMappingURL=ProdRoute.js.map