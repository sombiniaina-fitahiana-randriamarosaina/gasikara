"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Exception extends Error {
    constructor(message) {
        super(message);
        this.datas = {};
    }
    add(key, value) {
        this.datas[key] = value;
    }
    getDatas() {
        return this.datas;
    }
    static throwsException(key, value) {
        const e = new Exception("");
        e.add(key, value);
        throw e;
    }
}
exports.default = Exception;
//# sourceMappingURL=Exception.js.map