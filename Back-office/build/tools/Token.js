"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const HttpResponse_1 = require("./HttpResponse");
class Token {
    static control(req, res, next) {
        const { token } = req;
        jsonwebtoken_1.default.verify(token !== null && token !== void 0 ? token : "", Token.JWT_SECRET_KEY, (err, decoded) => {
            if (err) {
                const response = new HttpResponse_1.HttpApiResponse({ token }, 403, err.message);
                res.status(403).json(response.getResponse());
            }
            else if (Date.now() >= decoded.exp * 1000) {
                const response = new HttpResponse_1.HttpApiResponse({ token }, 403, "Token Expired");
                res.status(403).json(response.getResponse());
            }
            else {
                next();
            }
        });
    }
    static generate(object) {
        return jsonwebtoken_1.default.sign(object, Token.JWT_SECRET_KEY, { expiresIn: Token.JWT_EXPIRATION });
    }
}
Token.JWT_SECRET_KEY = "GASIKARA_SCRET_KEY";
Token.JWT_EXPIRATION = "24h";
exports.default = Token;
//# sourceMappingURL=Token.js.map