"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpApiResponse = void 0;
class HttpApiResponse {
    constructor(data, status = 200, message = "Ok", errors = null) {
        this.meta = {
            status,
            message,
            errors
        };
        this.data = data;
    }
    getResponse() {
        return {
            Meta: this.meta,
            Data: this.data,
        };
    }
}
exports.HttpApiResponse = HttpApiResponse;
//# sourceMappingURL=HttpResponse.js.map