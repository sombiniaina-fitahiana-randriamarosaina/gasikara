"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const StaticRoute_1 = __importDefault(require("./routes/StaticRoute"));
const ProdRoute_1 = __importDefault(require("./routes/ProdRoute"));
const app = (0, express_1.default)();
app.use((0, cors_1.default)()); // CORS
/*************** Extract Bearear token************************/
app.use((req, res, next) => {
    const bearerHeader = req.headers['authorization'];
    if (typeof bearerHeader != 'undefined' && bearerHeader.startsWith('Bearer')) {
        const bearer = bearerHeader.split(' ');
        req.token = bearer[1];
    }
    next();
});
/*************** Extract Bearear token************************/
app.get('/', (req, res) => {
    res.send('Hello, world! Gasikara');
});
app.use('/api/static', StaticRoute_1.default);
app.use('/api', ProdRoute_1.default);
app.listen(3000, () => {
    console.log(`Example app listening on port 3000`);
});
exports.default = app;
//# sourceMappingURL=app.js.map