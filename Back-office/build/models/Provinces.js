"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
class Provinces {
    constructor(id, name, description, area) {
        this.setId(id);
        this.setName(name);
        this.setDescription(description);
        this.setArea(area);
    }
    setId(id) {
        this.id = id;
    }
    getId() {
        return this.id;
    }
    setName(name) {
        this.name = name;
    }
    getName() {
        return this.name;
    }
    setDescription(description) {
        this.description = description;
    }
    getDescription() {
        return this.description;
    }
    setArea(area) {
        this.area = area;
    }
    getArea() {
        return this.area;
    }
    static findAll(pool) {
        return __awaiter(this, void 0, void 0, function* () {
            let values = [];
            const result = yield pool.query("SELECT * FROM Provinces");
            if (result.rows.length !== 0) {
                result.rows.forEach(province => {
                    values.push(new Provinces(province.idprovince, province.name, province.description, province.area));
                });
            }
            return values;
        });
    }
    static findById(pool, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const query = {
                text: 'SELECT * FROM Provinces WHERE IDPROVINCE = $1',
                values: [id],
            };
            const result = yield pool.query(query);
            if (result.rows.length !== 0) {
                let province = result.rows[0];
                return new Provinces(province.idprovince, province.name, province.description, province.area);
            }
            else {
                return undefined;
            }
        });
    }
}
exports.default = Provinces;
//# sourceMappingURL=Provinces.js.map