"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const crypto_js_1 = require("crypto-js");
const Exception_1 = __importDefault(require("../tools/Exception"));
class Users {
    constructor(id, name, password) {
        this.setId(id);
        this.setName(name);
        this.setPassword(password);
    }
    setId(id) {
        this.id = id;
    }
    getId() {
        return this.id;
    }
    setName(name) {
        this.name = name;
    }
    getName() {
        return this.name;
    }
    setPassword(password) {
        this.password = password;
    }
    getPassword() {
        return this.password;
    }
    login(pool) {
        var _a;
        return __awaiter(this, void 0, void 0, function* () {
            const query = {
                text: 'SELECT * FROM Users WHERE NAME = $1',
                values: [this.getName()],
            };
            const result = yield pool.query(query);
            if (result.rows.length === 0)
                Exception_1.default.throwsException("pseudo", "Utilisateur non trouvé !!");
            else if (result.rows[0].password !== (0, crypto_js_1.SHA1)((_a = this.getPassword()) !== null && _a !== void 0 ? _a : "").toString())
                Exception_1.default.throwsException("password", "Mot de passe incorrect !!");
            this.setId(result.rows[0].id);
        });
    }
    register(pool) {
        var _a, _b, _c;
        return __awaiter(this, void 0, void 0, function* () {
            // Controle
            if (!this.getName() || ((_a = this.getName()) === null || _a === void 0 ? void 0 : _a.trim().length) == 0)
                Exception_1.default.throwsException("pseudo", "Le champ pseudo doit être renseigné");
            if (!this.getPassword() || ((_b = this.getPassword()) === null || _b === void 0 ? void 0 : _b.trim().length) == 0)
                Exception_1.default.throwsException("password", "Le champ pseudo doit être renseigné");
            const serchQuery = {
                text: "SELECT * FROM Users WHERE NAME LIKE $1",
                values: [`%${this.getName()}%`],
            };
            const resultSearch = yield pool.query(serchQuery);
            if (resultSearch.rows.length > 0)
                Exception_1.default.throwsException("pseudo", "Pseudo déjà utilisé");
            // Insertion
            const query = {
                text: 'INSERT INTO USERS (NAME, PASSWORD) VALUES ($1, $2) RETURNING id',
                values: [this.getName(), (0, crypto_js_1.SHA1)((_c = this.getPassword()) !== null && _c !== void 0 ? _c : "").toString()]
            };
            const result = yield pool.query(query);
            this.setId(result.rows[0].id);
        });
    }
}
exports.default = Users;
//# sourceMappingURL=Users.js.map